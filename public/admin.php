<?php $data = json_decode(file_get_contents("../data/last_message.json"), true); ?>

<p>Dernier message :</p>
<ul>
    <li>Nom : <?= htmlentities(end($data)["name"]) ?></li>
    <li>Courriel : <?= htmlentities(end($data)["email"]) ?></li>
    <li>Message : <?= htmlentities(end($data)["message"]) ?></li>
</ul>
