<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="picnic.min.css" media="all" />
        <link rel="stylesheet" href="style.css" media="all" />
        <title>
            Mon site
        </title>
    </head>
    <body>
        <header>
            <?php getPart('menu'); ?>
        </header>
    </body>
</html>
