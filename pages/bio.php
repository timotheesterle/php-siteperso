<?php $data = getUserData("../data/user.json"); ?>

<table class="primary">
    <thead>
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Profession</th>
            <th>Expérience</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?= $data["name"] ?></td>
            <td><?= $data["first_name"] ?></td>
            <td><?= $data["occupation"] ?></td>
            <td>
                <ul>
                    <?php foreach ($data["experiences"] as $exp) {
                    echo "<li>{$exp["company"]} ({$exp["year"]})</li>";
                    } ?>
                </ul>
            </td>
        </tr>
    </tbody>
</table>
