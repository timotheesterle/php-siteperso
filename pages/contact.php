<form action="./save.php" method="POST">
    <label for="name">Nom :</label>
    <input id="name" type="text" name="name" placeholder="Votre nom" />
    <label for="email">Courriel :</label>
    <input
        id="email"
        type="email"
        name="email"
        placeholder="exemple@exemple.com"
    />
    <label for="message">Message :</label>
    <textarea
        id="message"
        name="message"
        placeholder="Votre message"
    ></textarea>
    <button type="submit">Envoyer</button>
</form>
